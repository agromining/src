"""Website_main_App URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static

from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView, RedirectView

from addresses.views import checkout_address_create_view, checkout_address_reuse_view
from accounts.views import SignUpView, FarmerSignUpView, CustomerSignUpView, FarmerSignUpView, CustomerSignUpView, LoginView, guest_register_view
from products.views import AddProductView3, FarmerListView

# LoginView, RegisterView

from .views import home_page
from predict.views import predictshow,SalesAjaxView

urlpatterns = [
    url(r'^$', home_page, name='home'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^register/guest/$', guest_register_view, name='guest_register'),
    # url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^signup/$', SignUpView.as_view(), name='signup'),
    url(r'^signup/farmer$', FarmerSignUpView.as_view(), name='farmer_signup'),
    url(r'^signup/customer$', CustomerSignUpView.as_view(), name='customer_signup'),
    url(r'^add-product$', AddProductView3.as_view(), name='add_product'),
    url(r'^products/', include("products.urls", namespace='products')),
    url(r'^my-products$', FarmerListView.as_view(), name='my_product'),
    url(r'^cart/', include("carts.urls", namespace='cart')),
    url(r'^checkout/address/create/$', checkout_address_create_view, name='checkout_address_create'),
    url(r'^checkout/address/reuse/$', checkout_address_reuse_view, name='checkout_address_reuse'),
    url(r'^admin/', admin.site.urls),
    url(r'^search/', include("search.urls", namespace='search')),
    # url(r'^predict/(?P<pk>\d+)/$',predictshow,name='predict'),
    url(r'^predict/$',predictshow,name='predictnoid'),
    url(r'^predict/api/(?P<pk>\d+)/$',SalesAjaxView.as_view(),name='predict-data'),
    url(r'^contact/', include("contact.urls", namespace='contact')),
    url(r'^paypal/', include("paypal.standard.ipn.urls")),
    url(r'^payment/', include("payment.urls", namespace='payment')),

]

if settings.DEBUG:
	urlpatterns =urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns =urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)