from django.conf.urls import url

from .views import (
        CartView, 
        ItemCountView,
        checkout_home,
        checkout_done_view
        )

urlpatterns = [
	url(r'^$', CartView.as_view(), name='cart'),
    url(r'^count/$', ItemCountView.as_view(), name='item_count'),
    url(r'^checkout/success/$', checkout_done_view, name='success'),
    url(r'^checkout/$', checkout_home, name='checkout'),
]

