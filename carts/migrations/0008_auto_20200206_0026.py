# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2020-02-05 18:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carts', '0007_auto_20200205_2331'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cart',
            old_name='products',
            new_name='cart_items',
        ),
    ]
