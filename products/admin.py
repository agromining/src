
from django.contrib import admin

from .models import Product, Variation

class VariationInline(admin.TabularInline):
	model = Variation
	extra = 0
	max_num = 10


class ProductAdmin(admin.ModelAdmin):
	search_fields = ['slug']
	list_display = ['__str__', 'title']
	inlines = [

		VariationInline,
	]
	class Meta:
		model = Product

admin.site.register(Product, ProductAdmin)
