import random
from django.db import models
from accounts.models import User
import os
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse

from Website_main_App.utils import unique_slug_generator

class ProductQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

class ProductManager(models.Manager):
    # def get_queryset(self):
    #     return ProductQuerySet(self.model, using=self._db)

    # def all(self):
    #     return self.get_queryset().active()

    # def featured(self): #Product.objects.featured() 
    #     return self.get_queryset().featured()

    def get_by_id(self, id):
        qs = self.get_queryset().filter(id=id) # Product.objects == self.get_queryset()
        if qs.count() == 1:
            return qs.first()
        return None

    def search(self, query):
        lookups = Q(title__icontains=query) | Q(description__icontains=query)
        return self.get_queryset().filter(lookups).distinct()

    def get_queryset(self):
        return ProductQuerySet(self.model, using=self._db)

    def all(self, *args, **kwargs):
        return self.get_queryset().active()

    # def search(self, query):
    #     return self.get_queryset().active().search(query)


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    # print(instance)
    #print(filename)
    new_filename = random.randint(1,3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "products/{new_filename}/{final_filename}".format(
            new_filename=new_filename, 
            final_filename=final_filename
            )


# Create your models here.
class Product(models.Model):
    title           = models.CharField(max_length=120)
    slug            = models.SlugField(blank=True, unique=True)
    description     = models.TextField()
    price           = models.DecimalField(decimal_places=2, max_digits=20, default=39.99)
    active          = models.BooleanField(default=True)
    price_unit      = models.CharField(max_length=120)
    added_by        = models.ForeignKey(User, default="agromining@gmail.com", verbose_name="Added by", on_delete=models.SET_DEFAULT)
    image           = models.ImageField(upload_to=upload_image_path, null=True, blank=True)
    timestamp       = models.DateTimeField(auto_now=True)
    availability    = models.BooleanField()


    REQUIRED_FIELDS = ['title', 'availabilty']
    
    # featured        = models.BooleanField(default=False)
    # active          = models.BooleanField(default=True)
    # timestamp       = models.DateTimeField(auto_now_add=True)


    objects = ProductManager()

    def get_absolute_url(self):
        return reverse("products:detail", kwargs={"slug": self.slug})

    def __str__(self):
        return self.slug

    # def __unicode__(self):
    #     return self.title

    # @property
    # def name(self):
    #     return self.title

    # def get_downloads(self):
    #     qs = self.productfile_set.all()
    #     return qs

def product_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(product_pre_save_receiver, sender=Product) 


# Variation for diffeerent variants of item.
# For example in khukhra there can be broileer and koiler. So need to add two products. Add two variants.
# But here not implemented 
# So if you consider to work with variation following things hould be remembered
# 1) Editing product form will change
# 2) Refer ecommerce2 by cfehome
# 3) def product_post_saved_receiver(sender, instance, created, *args, **kwargs): 
#       will be different(refer ecommerece 2 again)

class Variation(models.Model):
    product = models.ForeignKey(Product)
    title = models.CharField(max_length=120)
    price = models.DecimalField(decimal_places=2, max_digits=20)
    sale_price = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    active = models.BooleanField(default=True)
    inventory = models.IntegerField(null=True, blank=True) #refer none == unlimited amount

    def __str__(self):
        return str(self.product)

    def get_price(self):
        if self.sale_price is not None:
            return self.sale_price
        else:
            return self.price

    def get_html_price(self):
        if self.sale_price is not None:
            html_text = "<span class='sale-price'>%s</span> <span class='og-price'>%s</span>" %(self.sale_price, self.price)
        else:
            html_text = "<span class='price'>%s</span>" %(self.price)
        return mark_safe(html_text)

    def get_absolute_url(self):
        return self.product.get_absolute_url()

    def add_to_cart(self):
        return "%s?item=%s&qty=1" %(reverse("cart:cart"), self.id)

    def remove_from_cart(self):
        return "%s?item=%s&qty=1&delete=True" %(reverse("cart:cart"), self.id)

    def get_title(self):
        return "%s" %(self.product.title)

def product_post_saved_receiver(sender, instance, created, *args, **kwargs):

    
    product = instance

    Variation.objects.filter(product=product).delete()
    new_var = Variation()
    new_var.product = product
    new_var.title = "Default"
    new_var.price = product.price
    new_var.save()


post_save.connect(product_post_saved_receiver, sender=Product)