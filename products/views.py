from django.views.generic import ListView, DetailView
from django.contrib.auth import authenticate, login
from django.shortcuts import render
from django.views.generic import CreateView, FormView, DetailView, View, UpdateView, TemplateView, ListView, DeleteView
from django.utils.http import is_safe_url
from django.shortcuts import render,redirect
from carts.models import Cart
from .models import Product
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import Http404
from .forms import ProductEditForm, ProductEditForm2
from django.views.generic.edit import ModelFormMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin



class MultipleObjectMixin(object):
    def get_object(self, queryset=None, *args, **kwargs):
        slug = self.kwargs.get("slug")
        if slug:
            try:
                obj = self.model.objects.get(slug=slug)
            except self.model.MultipleObjectsReturned:
                obj = self.get_queryset().first()
            except:
                raise Http404
            return obj
        raise Http404

# Create your views here.
# class AddProductView(CreateView): 
# 			template_name = 'products/add.html'

# 			def get(self, request):
# 				form	= ProductEditForm
# 				return render(request, self.template_name, {'form': form})

# 			def post(self, request):
# 				form 	= ProductEditForm(request.POST)

# 				if form.is_valid():
# 					product 	= form.save(commit=False)
# 					product.added_by	= request.user
# 					product.save()
# 					text	= form.cleaned_data['product']
# 					form 	= ProductEditForm
# 					return redirect('/')

# 				args 	= {'form': form, 'text': text}
# 				return render(request, self.template_name, args)

# # class AddProductView2(CreateView):
# class AddProductView2(CreateView):
# 	    	model = Product
#     		form_class = ProductEditForm
#     		template_name = 'products/add.html'
#     		def get_context_data(self, **kwargs):
#     			kwargs['user_type'] = 'customer'
#     			return super().get_context_data(**kwargs)

#     		def form_valid(self, form):
#     			product = form.save()
#     			return redirect('/')

class AddProductView3(CreateView):
    template_name = 'products/add.html'

    def get(self, request):
        form = ProductEditForm2()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = ProductEditForm2(request.POST, request.FILES)
        if form.is_valid():
            product = form.save(commit=False)
            product.added_by = request.user 
            product.save()
            form = ProductEditForm2()
            messages.success(request, 'Product Added')
            return redirect('/add-product', message='Product Added')

        args = {'form': form}
        return render(request, self.template_name, args)

class ProductListView(ListView):
    template_name = "products/list.html"

    # def get_context_data(self, *args, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*args, **kwargs)
    #     print(context)
    #     return context

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.all()

class ProductDetailSlugView(DetailView):
    queryset = Product.objects.all()
    template_name = "products/detail.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailSlugView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context
    def get_object(self, *args, **kwargs):
        request = self.request
        slug = self.kwargs.get('slug')

        #instance = get_object_or_404(Product, slug=slug, active=True)
        try:
            instance = Product.objects.get(slug=slug)
        except Product.DoesNotExist:
            raise Http404("Not found..")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug=slug)
            instance = qs.first()
        except:
            raise Http404("Exceptional Error ")
        return instance

# class ProductDetailSlugView(DetailView):
#     queryset = Product.objects.all()
#     template_name = "products/detail.html"

#     def get_context_data(self, *args, **kwargs):
#         context = super(ProductDetailSlugView, self).get_context_data(*args, **kwargs)
#         user_access     = "sdasd"
#         context["user_accessing"] = user_access
#         return context

#     def get_object(self, *args, **kwargs):
#         request = self.request
#         slug = self.kwargs.get('slug')

#         #instance = get_object_or_404(Product, slug=slug, active=True)
#         try:
#             instance = Product.objects.get(slug=slug)
#         except Product.DoesNotExist:
#             raise Http404("Not found..")
#         except Product.MultipleObjectsReturned:
#             qs = Product.objects.filter(slug=slug)
#             instance = qs.first()
#         except:
#             raise Http404("Exceptional Error ")
#         return instance

class FarmerListView(ListView):
    template_name = "products/list.html"
    # added_by = 'abcfarmer@gmail.com'
    # qs      = Product.objects.filter( added_by__email__icontains= added_by )



    # def get_context_data(self, *args, **kwargs):
    #     context = super(ProductListView, self).get_context_data(*args, **kwargs)
    #     return context

    def get_queryset(self):
        request = self.request
        added_by = self.request.user
        qs      = Product.objects.filter( added_by__email__icontains= added_by )

        return qs

class ProductEditView(MultipleObjectMixin, UpdateView):
    model = Product
    form_class = ProductEditForm2
    template_name = 'products/edit.html'
  

    def get_context_data(self, *args, **kwargs):
        context = super(ProductEditView, self).get_context_data(*args, **kwargs)
        return context

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, 'Edit success')
        slug = self.kwargs.get('slug')
        return reverse("products:detail", kwargs={'slug':slug })


class ProductDeleteView(DeleteView):
    model = Product
    success_url = reverse_lazy("add_product")
    success_message = "Product was deleted successfully."

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super(ProductDeleteView, self).delete(request, *args, **kwargs)




		
	
			
			
